const express = require('express');
const router = express.Router();
const debug = require('debug')('calculadora-cms2.0:locationsRouter');
const {Device, Location} = require('../data/models');
const moment = require('moment-timezone');
moment.tz.setDefault("America/Santo_Domingo");


router.get('/all', (req, res, next)=>{

    Location.findAll({
        attributes: ['longitude', 'latitude', 'utcTimeFix', 'accuracy'],
        order: [['createdAt', "DESC"]],
        include: [Device]})
        .then(locations => {

            const locs = locations.map(item=> {
                item.dataValues.utcTimeFix = moment(Number(item.dataValues.utcTimeFix))
                    .format("DD-MMM-YY hh:mm a");
                return item
            });

            let context = {
                title: "All locations",
                locations: locs,
            };
            res.render('locations/all', context);
        })
        .catch(err=>{
            debug(err);
        });

});



module.exports = router;