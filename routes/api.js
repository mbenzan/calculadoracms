const express = require('express');
const router = express.Router();
const debug = require('debug')('calculadora-cms2.0:api');
const {Location} = require('../data/models/');
const {Device} = require('../data/models/');


router.post('/register', function(req, res, next) {
    Device.create({
        owner: req.body.owner,
        dev_uuid: req.body.uuid
    })
        .then(dev =>{
            res.setHeader('Content-Type', 'application/json');
            res.json({
                "status_code": 200,
                "identifier": dev.identifier,
                "ack": true
            });
        })
        .catch(err=>{
            res.setHeader('Content-Type', 'application/json');
            res.json({
                "status_code": 500,
                "ack": false,
                "identifier": null,
                "err": err
            });
        });

});

router.post('/update', (req, res, next)=>{

    Location.create({
        accuracy: req.body.accuracy,
        altitude: req.body.altitude,
        date: req.body.date,
        latitude: req.body.lat,
        longitude: req.body.lon,
        provider: req.body.provider,
        utcTimeFix: req.body.utcFixTime,
        DeviceIdentifier: req.body.identifier
    })
        .then(loc => {
            res.setHeader('Content-Type', 'application/json');
            res.json({
                "status_code": 200,
                "ack": !!loc.id,
                "err": []
            });
        })
        .catch(err => {
            debug(err);
            res.setHeader('Content-Type', 'application/json');
            res.json({
                "status_code": 500,
                "ack": false,
                "err": err
            });
        });

});

router.all('/alive', (req, res, next)=>{
   res.send("ack");
});

module.exports = router;
