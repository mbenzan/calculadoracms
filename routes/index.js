const express = require('express');
const router = express.Router();
const debug = require('debug')('calculadora-cms2.0:api');
const apiRouter = require('./api');
const {Device, Location} = require('../data/models');
const locationsRouter = require('./locations');
const devicesRouter = require('./devices');

//All other routes
router.use('/api', apiRouter);
router.use('/locations', locationsRouter);
router.use('/devices',devicesRouter);

/* GET home page. */
router.get('/', function(req, res, next) {

    Location.findAll({
        attributes: ['longitude', 'latitude', 'utcTimeFix', 'accuracy', 'altitude'],
        limit: 100,
        order: [['createdAt', "DESC"]],
        include: [Device]})
        .then(locations => {
            let context = {
                title: "Dashboard",
                lastLocations: locations.slice(0, 6),
                locations: locations,
            };
            res.render('index', context);
        })
        .catch(err=>{
            debug(err);
        });

});


router.get('/install', (req, res, next)=>{
    res.render('install', {
        title: "Install Instructions"
    });
});



module.exports = router;
