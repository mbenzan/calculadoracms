const express = require('express');
const router = express.Router();
const {Device} = require('../data/models');
const moment = require('moment-timezone');
moment.tz.setDefault("America/Santo_Domingo");


router.get('/all', (req, res, next)=>{

    Device.findAll({
        attributes: ["owner", "dev_uuid", "identifier", "createdAt"],
        order: [['createdAt', "DESC"]],
    })
        .then(devices=>{
            let context = {
                title: "All devices",
                devices: devices
            };

            res.render('devices/all', context);
        });


});


module.exports = router;