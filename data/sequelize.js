const Sequelize = require('sequelize');
const debug = require('debug')('calculadora-cms2.0:sequelize.js');

const DB_NAME = "CalcCMS";

let sequelize;
debug("Is production: " + process.env.IS_PRODUCTION + "; type: " + typeof process.env.IS_PRODUCTION);

if(process.env.IS_PRODUCTION === '1'){

    //Production config
    const db = process.env.MYSQL_DATABASE_NAME;
    const usr = process.env.MYSQL_USERNAME;
    const pss = process.env.MYSQL_PASSWORD;
    const url = process.env.MYSQL_URL;
    sequelize = new Sequelize(db, usr, pss, {
        dialect: 'mysql',
        host: url,
        operatorsAliases: false,

    });

    debug("Using production sequelize config!");

} else {

    //Dev config
    sequelize = new Sequelize(DB_NAME, null, null, {
        dialect: 'sqlite',
        storage: './db.sqlite',
        operatorsAliases: false,
    });

    debug("Using development sequelize config!");
}



sequelize
    .authenticate()
    .then(err => {
      debug('Connection has been started correctly');
    }, err => {
      debug(`Connection could not be created. Err: ${err}`);
    });


module.exports = sequelize;
