import {
  GraphQLObjectType as ObjectType,
  GraphQLString as StringType,
  GraphQLInt as IntergerType,
  GraphQLNonNull as NonNull,
} from 'graphql';

const DeviceType = new ObjectType({
  name: 'Device',
  fields: {
    id: { type: new NonNull(IntergerType) },
    deviceUUID: { type: new NonNull(StringType) },
    identifier: { type: new NonNull(StringType) },
    owner: { type: new NonNull(StringType) },

  },
});

export default DeviceType;
