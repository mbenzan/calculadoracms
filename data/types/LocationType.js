import {
  GraphQLObjectType as ObjectType,
  GraphQLString as StringType,
  GraphQLFloat as FloatType,
  GraphQLInt as IntergerType,
  GraphQLNonNull as NonNull,
} from 'graphql';

const LocationType = new ObjectType({
  name: 'Location',
  fields: {
    id: { type: new NonNull(IntergerType) },
    accuracy: { type: new NonNull(FloatType) },
    altitude: { type: new NonNull(FloatType) },
    latitude: { type: new NonNull(FloatType) },
    longitude: { type: new NonNull(FloatType) },
    provider: { type: new NonNull(StringType) },
    utcTimeFix: { type: new NonNull(StringType) },

  },
});

export default LocationType;
