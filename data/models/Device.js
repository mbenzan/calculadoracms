const {DataTypes} = require('sequelize');
const Model = require('../sequelize');


const Device = Model.define('Device', {
  owner: { type: DataTypes.STRING, allowNull: false },
  dev_uuid: { type: DataTypes.STRING },
  identifier: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV1, primaryKey: true },
});

module.exports =  Device;
