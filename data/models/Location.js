const {DataTypes} = require('sequelize');
const Model = require('../sequelize');
const moment = require('moment');
const debug = require('debug')('calculadora-cms2.0:Location.js');


const Location = Model.define('Location', {
  accuracy: { type: DataTypes.FLOAT, allowNull: false },
  altitude: { type: DataTypes.FLOAT, allowNull: false },
  date: { type: DataTypes.STRING, allowNull: false },
  provider: { type: DataTypes.STRING, allowNull: false },
  utcTimeFix: { type: DataTypes.BIGINT, allowNull: false },
  latitude: {
    type: DataTypes.FLOAT,
    allowNull: true,
    defaultValue: null,
    validate: { min: -90, max: 90 },
  },
  longitude: {
    type: DataTypes.FLOAT,
    allowNull: true,
    defaultValue: null,
    validate: { min: -180, max: 180 },
  },
}, {
  validate: {
    bothCoordsOrNone() {
      if ((this.latitude === null) !== (this.longitude === null)) {
        throw new Error('Require either both latitude and longitude or neither');
      }
    },
  },
});


Location.afterFind((locs) => {
    if(locs.constructor === Array) {
        locs.map(loc=>{
            // debug("Location data: " + Number(loc.utcTimeFix) + " type: " + typeof Number(loc.utcTimeFix));

            loc.dataValues.stringTime = moment(Number(loc.utcTimeFix)).fromNow();
            return loc;
        });
    } else {
        locs.dataValues.stringTime = moment(Number(locs.utcTimeFix)).fromNow();
    }
    return locs;
});
module.exports = Location;
