/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

const sequelize = require('../sequelize');
const Device = require('./Device');
const Location = require('./Location');


const debug = require('debug')('calculadora-cms2.0:models:index.js');


function sync(...args) {
  return sequelize.sync(...args);
}


//Make the associations.
Device.hasMany(Location, {as: "Locations"});
Location.belongsTo(Device);

// Sync the data

sync({ force: false })
  .then(err => {
    debug('Tables synced!');
  })
  .catch(err => {
    debug(`Unable to sync tables. Err: ${err}`);
  });


module.exports =  { sync, Device, Location };
