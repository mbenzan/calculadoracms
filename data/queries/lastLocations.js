import LocationType from '../types/LocationType';
import Location from '../models/Location';
import { GraphQLList } from 'graphql';

const lastLocations = {
  type: new GraphQLList(LocationType),
  async resolve({ request }) {
    const data = await Location.findAll({
      limit: 25,
      order: [['createdAt', 'DESC']],
    });

    return data;
  },
};

export default lastLocations;
